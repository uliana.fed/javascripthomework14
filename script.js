function displayList(array, parent = document.body) {
    const list = document.createElement("ul");
  
    array.forEach(item => {
      const listItem = document.createElement("li");
      listItem.textContent = item;
      list.appendChild(listItem);
    });
  
    parent.appendChild(list);
  }
  
  const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  displayList(array1);
  
  const array2 = ["1", "2", "3", "sea", "user", 23];
  const parentElement = document.getElementById("taskContainer");
  displayList(array2, parentElement);

const body = document.body;
const wrapper = document.querySelector('.wrapper');
const buttons = document.querySelectorAll('button');

const savedTheme = localStorage.getItem('theme');

function changeTheme() {
  if (body.classList.contains('dark-theme')) {
    body.classList.remove('dark-theme');
    wrapper.classList.remove('dark-theme');
    buttons.forEach(button => button.classList.remove('dark-theme'));
    localStorage.setItem('theme', 'light');
  } else {
    body.classList.add('dark-theme');
    wrapper.classList.add('dark-theme');
    buttons.forEach(button => button.classList.add('dark-theme'));
    localStorage.setItem('theme', 'dark');
  }
}

const changeThemeButton = document.createElement('button');
changeThemeButton.textContent = 'Змінити тему';
changeThemeButton.addEventListener('click', changeTheme);
wrapper.appendChild(changeThemeButton);

if (savedTheme === 'dark') {
  changeTheme();
}